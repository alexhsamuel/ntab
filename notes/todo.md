- Support slicing by container in any object that is indexed by name.
  - `_Cols` view objects.
  - `_Row`

- Either require fixfmt (and get rid of `lib._text`) or not.  Or vendor it...?


# Fun Features

- CSV / delimited file reader
  - type conversion
    - ora types
  - auto type inference
    - ora types
    - small int types
    - categorical

- categorical array--custom ndarray subclass?
  - auto categorical

- ntab-style interactive console table browser

- fast ODBC, Postgres bulk load drivers

